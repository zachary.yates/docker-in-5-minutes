# Docker in 5 minutes

1. What is a container?  
LxC process segmentation

1. What is an image?  
Images, snapshots, trees (hashes)
Layered file system

1. How does docker fit into all of this?  
Dockerfiles, and building images
commiting changes from a container to an image
Registry, the docker client, the docker daemon    

1. Demo / Running / Debugging